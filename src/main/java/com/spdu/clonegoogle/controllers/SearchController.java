package com.spdu.clonegoogle.controllers;

import com.spdu.clonegoogle.services.PaginationService;
import com.spdu.clonegoogle.services.SearchService;
import lombok.AllArgsConstructor;
import org.apache.lucene.document.Document;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/")
@AllArgsConstructor
@Validated
public class SearchController {

    private SearchService searchService;
    private PaginationService paginationService;

    private static final String SEARCH_RELEVANCE_FIELD = "relevance";

    @GetMapping
    public String getSearchPage(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                @RequestParam(value = "countDocuments", required = false, defaultValue = "0") Integer countDocuments,
                                Model model) {
        model.addAttribute("countOfPages", page);
        model.addAttribute("countDocuments", countDocuments);
        model.addAttribute("keywords", "");

        return "search";
    }

    @GetMapping("/search")
    public String searchDocuments(@RequestParam @NotBlank @NotNull String keywords,
                                  @RequestParam(value = "sort", required = false, defaultValue = SEARCH_RELEVANCE_FIELD) String sort,
                                  @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                  Model model) {
        List<Document> documents = searchService.searchDocuments(keywords, sort);
        model.addAttribute("countDocuments", documents.size());
        model.addAttribute("sort", sort);

        List<Document> pageDocuments = paginationService.getDocumentsInCurrentPage(page, documents);
        model.addAttribute("documents", pageDocuments);

        int countOfPages = paginationService.getCountOfPages(documents.size());
        model.addAttribute("countOfPages", countOfPages);
        model.addAttribute("keywords", keywords);

        return "search";
    }
}
