package com.spdu.clonegoogle.controllers;

import com.spdu.clonegoogle.services.PageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Min;

@Controller
@RequestMapping("/index")
@AllArgsConstructor
@Validated
public class IndexController {

    private PageService pageService;

    @GetMapping
    public String getIndexPage() {
        return "index";
    }

    @PostMapping
    public String indexingPage(@RequestParam String url,
                               @RequestParam(required = false, defaultValue = "1") @Min(1) Integer searchPageDepth,
                               Model model) {
        pageService.findDocumentsByLink(url, searchPageDepth);

        model.addAttribute("finishIndexing", "Page indexing complete.");

        return "index";
    }
}
