package com.spdu.clonegoogle.services;

import com.spdu.clonegoogle.configuration.SpringConfiguration;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Paths;

@Service
@AllArgsConstructor
@Slf4j
public class IndexService {

    private StandardAnalyzer standardAnalyzer;
    private SpringConfiguration springConfiguration;

    public void addDocumentToIndex(String link,
                                   org.jsoup.nodes.Document doc) {
        try (Directory directory = FSDirectory.open(Paths.get(springConfiguration.getIndexPath()))) {
            if (doc == null || link == null) {
                return;
            }

            String title = doc.title();
            Element bodyElement = doc.body();

            if (title == null || bodyElement == null) {
                return;
            }

            String body = bodyElement.text();
            addFieldsToIndex(link, directory, title, body);
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
    }

    private void addFieldsToIndex(String link,
                                  Directory directory,
                                  String title,
                                  String body) {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(standardAnalyzer);

        try (IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig)) {
            Document document = new Document();

            document.add(new TextField("title", title, Field.Store.YES));
            document.add(new TextField("body", body, Field.Store.YES));
            document.add(new TextField("link", link, Field.Store.YES));
            document.add(new SortedDocValuesField("title", new BytesRef(title)));

            indexWriter.addDocument(document);
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
    }
}
