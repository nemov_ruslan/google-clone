package com.spdu.clonegoogle.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class PageService {

    private IndexService indexService;
    private Set<String> linksFromSession;

    private static final int READ_TIMED_OUT = 3 * 1000;

    public void findDocumentsByLink(String link,
                                     int searchPageDepth) {
        try {
            searchPageDepth--;
            if (link == null) {
                return;
            }
            Document document = Jsoup
                    .connect(link.toLowerCase())
                    .method(Connection.Method.GET)
                    .timeout(PageService.READ_TIMED_OUT)
                    .followRedirects(true)
                    .get();

            if (document == null || document.title() == null || document.body() == null) {
                return;
            }
            linksFromSession.add(link);

            indexService.addDocumentToIndex(link, document);

            if (searchPageDepth > 0) {
                Set<String> originalLinksOnPage = new HashSet<>(findOriginalLinksInDocument(document));

                for (String originalLink : originalLinksOnPage) {
                    findDocumentsByLink(originalLink, searchPageDepth);
                }
            }
        } catch (IOException | IllegalArgumentException ex) {
            log.error(ex.getMessage());
        }
    }

    private Set<String> findOriginalLinksInDocument(org.jsoup.nodes.Document document) {
        Set<String> linksOnPage = new HashSet<>();
        Elements links = document.select("a");

        for (Element element : links) {
            String link = element.attr("abs:href");

            if (!link.isBlank() && !link.isEmpty() && !linksFromSession.contains(link)) {
                linksOnPage.add(link);
            }
        }
        return linksOnPage;
    }
}
