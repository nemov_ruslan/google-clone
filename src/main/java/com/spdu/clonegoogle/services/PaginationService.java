package com.spdu.clonegoogle.services;

import org.apache.lucene.document.Document;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaginationService {

    private static final int DOCUMENTS_PER_PAGE = 10;

    public int correctCurrentPage(int page,
                                  List<Document> documents) {
        int emptyDocumentsPage = 1;
        if (documents == null || documents.isEmpty()) {
            return emptyDocumentsPage;
        }

        double pages = (double) documents.size() / DOCUMENTS_PER_PAGE;
        int countPages = (int) Math.ceil(pages);

        return Math.min(page, countPages);
    }

    public List<Document> getDocumentsInCurrentPage(int page,
                                                    List<Document> documents) {
        List<Document> pageDocuments = new ArrayList<>();
        if (documents == null) {
            return pageDocuments;
        }

        int currentPage = correctCurrentPage(page, documents);
        int numberFirstDocumentInPage = currentPage * DOCUMENTS_PER_PAGE - DOCUMENTS_PER_PAGE;
        int numberLastDocumentInPage = numberFirstDocumentInPage + DOCUMENTS_PER_PAGE;

        if (numberLastDocumentInPage > documents.size()) {
            numberLastDocumentInPage = documents.size();
        }

        for (int i = numberFirstDocumentInPage; i < numberLastDocumentInPage; i++) {
            pageDocuments.add(documents.get(i));
        }

        return pageDocuments;
    }

    public int getCountOfPages(int countOfDocuments) {
        int countOfPages = countOfDocuments / DOCUMENTS_PER_PAGE;

        if (countOfDocuments < DOCUMENTS_PER_PAGE) {
            countOfPages++;
        } else if (countOfDocuments % DOCUMENTS_PER_PAGE > 0) {
            countOfPages++;
        }

        return countOfPages;
    }
}
