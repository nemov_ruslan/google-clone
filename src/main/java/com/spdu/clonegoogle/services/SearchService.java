package com.spdu.clonegoogle.services;

import com.spdu.clonegoogle.configuration.SpringConfiguration;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class SearchService {

    private StandardAnalyzer standardAnalyzer;
    private SpringConfiguration springConfiguration;

    private static final String SEARCH_ALPHABETICALLY_FIELD = "alphabetically";
    private static final String SEARCH_FIELD = "body";
    private static final String SORT_FIELD = "title";

    public List<Document> searchDocuments(String queryString,
                                          String sortType) {
        if (sortType.equals(SEARCH_ALPHABETICALLY_FIELD)) {
            Sort sort = new Sort(new SortField(SORT_FIELD, SortField.Type.STRING));
            return searchSortedDocuments(queryString, sort);
        } else {
            return searchSortedDocuments(queryString, Sort.RELEVANCE);
        }
    }

    private List<Document> searchSortedDocuments(String queryString,
                                                 Sort sort) {
        List<Document> documents = new ArrayList<>();
        try (Directory directory = FSDirectory.open(Paths.get(springConfiguration.getIndexPath()));
             IndexReader indexReader = DirectoryReader.open(directory)) {

            QueryParser parser = new QueryParser(SEARCH_FIELD, standardAnalyzer);
            parser.setDefaultOperator(QueryParser.Operator.AND);
            parser.setPhraseSlop(0);
            Query query = parser.createPhraseQuery(SEARCH_FIELD, queryString);

            IndexSearcher searcher = new IndexSearcher(indexReader);

            TopDocs topDocs = searcher.search(query, Integer.MAX_VALUE, sort);

            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                documents.add(searcher.doc(scoreDoc.doc));
            }
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        return documents;
    }
}
