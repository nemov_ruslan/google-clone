package com.spdu.clonegoogle.exceptions;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.net.SocketTimeoutException;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({SocketTimeoutException.class})
    public String handleSocketTimeoutException(final SocketTimeoutException exception, Model model) {
        String errorMessage =  exception.getMessage() + ". Reading time for this link is over.";
        model.addAttribute("errorMessage", errorMessage);
        return "errorPage";
    }

    @ExceptionHandler({NullPointerException.class})
    public String handleNullPointerException(final NullPointerException exception, Model model) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "errorPage";
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public String handleConstraintViolationException(final ConstraintViolationException exception, Model model) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "errorPage";
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public String handleMethodArgumentTypeMismatchException(final MethodArgumentTypeMismatchException exception, Model model) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "errorPage";
    }
}
