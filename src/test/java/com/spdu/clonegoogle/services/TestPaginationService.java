package com.spdu.clonegoogle.services;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestPaginationService {

    private PaginationService paginationService;

    @BeforeEach
    private void createInstancePaginationService() {
        paginationService = new PaginationService();
    }

    @Test
    public void testCorrectCurrentPageForCountDocumentsLessTen() {

        Document documentWithBody = new Document();
        Document documentWithTitle = new Document();

        String body = "Object-oriented programming uses objects, but not all of the associated techniques " +
                "and structures are supported directly in languages that claim to support OOP.";
        String title = "Object-oriented programming";

        Field bodyField = new StringField("body", body, Field.Store.YES);
        Field titleField = new StringField("title", title, Field.Store.YES);

        documentWithBody.add(bodyField);
        documentWithTitle.add(titleField);

        List<Document> documents = new ArrayList<>(Arrays.asList(documentWithBody, documentWithTitle));
        int page = 1;

        assertEquals(paginationService.correctCurrentPage(page, documents), page);
    }

    @Test
    public void testCorrectCurrentPageForCountDocumentsMoreThanTen() {
        int countDocuments = 12;
        List<Document> documents = getDocuments(countDocuments);
        int page = 2;

        assertEquals(paginationService.correctCurrentPage(page, documents), page);
    }

    @Test
    public void testCorrectCurrentPageForDocumentsIsNull() {
        int page = 10;
        int emptyDocumentsPage = 1;

        assertEquals(paginationService.correctCurrentPage(page, null), emptyDocumentsPage);
    }

    @Test
    public void testCorrectCurrentPageForDocumentsIsEmpty() {
        int page = 10;
        int emptyDocumentsPage = 1;
        List<Document> documents = new ArrayList<>();

        assertEquals(paginationService.correctCurrentPage(page, documents), emptyDocumentsPage);
    }

    @Test
    public void testGetCountOfPagesForCountOfDocumentsLessThanZero() {
        int countOfDocuments = -1;
        int countOfPages = 1;

        assertEquals(paginationService.getCountOfPages(countOfDocuments), countOfPages);
    }

    @Test
    public void testGetCountOfPagesForCountOfDocumentsMoreThanTen() {
        int countOfDocuments = 16;
        int countOfPages = 2;

        assertEquals(paginationService.getCountOfPages(countOfDocuments), countOfPages);
    }

    @Test
    public void testGetDocumentsInCurrentPageForCountOfDocumentsMoreThanTen() {
        int countDocumentsInput = 10;
        List<Document> documentsInput = getDocuments(countDocumentsInput);

        int countDocumentsOutput = 3;
        List<Document> documentsOutput = getDocuments(countDocumentsOutput);

        documentsInput.addAll(documentsOutput);
        int currentPage = 2;

        assertEquals(paginationService.getDocumentsInCurrentPage(currentPage, documentsInput), documentsOutput);
    }

    @Test
    public void testGetDocumentsInCurrentPageForDocumentsIsEmpty() {
        List<Document> documentsInput = new ArrayList<>();
        List<Document> documentsOutput = new ArrayList<>();
        int currentPage = 1;

        assertEquals(paginationService.getDocumentsInCurrentPage(currentPage, documentsInput), documentsOutput);
    }

    private List<Document> getDocuments(int countDocumentsInput) {
        List<Document> documents = new ArrayList<>();

        for (int i = 0; i < countDocumentsInput; i++) {
            String title = "Object-oriented programming";
            Document document = new Document();
            document.add(new StringField("title", title, Field.Store.YES));
            documents.add(document);
        }

        return documents;
    }
}
