Google clone with using Lucene.

Algorithm of work:

Indexing

1. Open /index page.
2. Pass url, search page depth and press Index button.
3. Indexing this page and links in this page is started (number of links depending on page depth).
4. After index will show the message "Page indexing complete.".

Search

1. Open / page.
2. Write keywords.
3. Press Search button.
4. You have posible to change sort and page.